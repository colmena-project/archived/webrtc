# How to run the marvelous:

make dev-run
poetry install --no-dev --no-root
poetry run python3 -m talked --host 0.0.0.0

# start recording and streaming
curl -X POST http://172.17.0.2:5000/start -H 'Content-Type:application/json' -d '{"token":"4xhsmnm7","nextcloud_version":"23","audio_only":"True","grid_view":"False","enable_streaming":"True","streaming_url":"icecast://source:d2livreah68688@sdrtelecom.com.br:8000/teste.webm"}'


# to start recording:
curl -X POST http://172.17.0.2:5000/start -H 'Content-Type:application/json' -d '{"token":"4xhsmnm7","nextcloud_version":"23","audio_only":"True","grid_view":"False","enable_streaming":"True","streaming_url":"icecast://source:d2livreah68688@sdrtelecom.com.br:8000/teste.webm"}'

# to stop recording:
curl -X POST http://172.17.0.2:5000/stop -H 'Content-Type:application/json' -d '{"token":"4xhsmnm7"}'

# get status:
curl -X POST http://172.17.0.2:5000/status -H 'Content-Type:application/json' -d '{"token":"4xhsmnm7"}'

# get version:
curl -X GET http://172.17.0.2:5000/

# More information...

Recorded file has the following format, with timestamp:
20220113T123553_output.opus

There is a configuration file parameter 'finalise_recording_script' which
calls a script after the recording... we could use it to add the file to the
proper NC location.
Also,  'recording_dir' is another configuration file parameter which can be
useful.
