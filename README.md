# Webrtc

This project contains sotware for the Maia's WebRTC backend

## Standard References:

- WebRTC 1.0: Real-Time Communication Between Browsers, W3C Recommendation,
  26 January 2021:  https://www.w3.org/TR/webrtc/

-WebRTC Priority Control API, W3C Candidate Recommendation Snapshot,
  18 March 2021: https://w3c.github.io/webrtc-priority/

- Explanation of the API: https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Signaling_and_video_calling

- A/V Codecs supports: https://developer.mozilla.org/en-US/docs/Web/Media/Formats/WebRTC_codecs

- Session Description Protocol (SDP) Offer/Answer procedures for Interactive
  Connectivity Establishment (ICE): https://tools.ietf.org/id/draft-ietf-mmusic-ice-sip-sdp-39.html

### WebRTC solutions for Next Cloud

The default WebRTC Next-Cloud WebRTC implementation:
https://github.com/nextcloud/spreed

Spreed "standard" WebRTC documentation:
https://github.com/nextcloud/spreed/tree/master/docs

Integration with NextCloud High Performance Backend (Janus + external
signaling server) guide:
https://markus-blog.de/index.php/2020/11/20/how-to-run-nextcloud-talk-high-performance-backend-with-stun-turnserver-on-ubuntu-with-docker-compose/

Janus is an open source, general purpose, WebRTC server and SFU:
https://janus.conf.meetecho.com/

NextCloud RTC signaling server for Janus:
https://github.com/strukturag/nextcloud-spreed-signaling

Janus source code: https://github.com/meetecho/janus-gateway

Janus deploy guide: https://janus.conf.meetecho.com/docs/deploy.html

Coturn STUN server: https://github.com/coturn/coturn

Configuration of DNS for the STUN server: https://ourcodeworld.com/articles/read/1175/how-to-create-and-configure-your-own-stun-turn-server-with-coturn-in-ubuntu-18-04

- Other alternatives for Next Cloud WebRTC functionaluty are the Big Blue Button (
  https://apps.nextcloud.com/apps/bbb ), Jitsi (
  https://apps.nextcloud.com/apps/jitsi ).

## Connecting to a WebRTC conference for recording an live streaming purposes

- gst-launch samples to connect to WebRTC:
https://developer.ridgerun.com/wiki/index.php?title=GstWebRTC_-_Opus_Examples#Send-Receive_Pipeline

- Script to connect to a Janus RTC room:
https://gitlab.freedesktop.org/gstreamer/gst-examples/-/blob/master/webrtc/janus/janusvideoroom.py

- Example by one gstreamer webrtc author on how to connect to a WebRTC peer:
  http://blog.nirbheek.in/2018/02/gstreamer-webrtc.html

- Debugging WebRTC:
https://testrtc.com/webrtc-internals-parameters/
https://testrtc.com/find-webrtc-active-connection/

## Gstreamer API references

- Comments about an implementation using gstreamer webrtc implementation: https://aweirdimagination.net/2020/07/05/gstreamer-webrtc/

- Comments about API usage: https://cloud.google.com/architecture/gpu-accelerated-streaming-using-webrtc#gstreamer_pipeline

## Other references

- Tons of references about Webrtc, p2p communication, and related topics: https://github.com/kgryte/awesome-peer-to-peer

## Implementations

### Gstreamer-based samples

https://gitlab.freedesktop.org/gstreamer/gst-examples/-/blob/master/webrtc/README.md

https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad/-/tree/1.18/tests/examples/webrtc

https://git.aweirdimagination.net/perelman/minimal-webrtc-gstreamer

https://git.aweirdimagination.net/perelman/minimal-webrtc

and gstreamer (in gst-plugins-bad) webrtc source...

### Generic WebRTC related samples

- Very interesting software for audio conferencing:
  https://github.com/MixinNetwork/kraken.fm and https://github.com/MixinNetwork/kraken

- A WebRTC implementation (used by Kraken): https://github.com/pion/webrtc

- Tool to record media from many sources: https://github.com/muaz-khan/RecordRTC

## Code (WHAT WE WILL USE!)

We will used talked, which proceeds the a NC WebRTC call recording by using
a firefox (gecko) engine to connect to the call website and record the call.

- Directory 'talked' contains samples and how to use talked. Official git of
  talked:
https://github.com/MetaProvide/talked

- Colmena customizations for talked (streaming support added!):
https://github.com/ColmenaDev/talked

## Authors

Rafael Diniz (rafael@riseup.net)

## License

This code is licensed under The GNU General Public License v3.0, or any
higher version.
